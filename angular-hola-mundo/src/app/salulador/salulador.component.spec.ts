import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaluladorComponent } from './salulador.component';

describe('SaluladorComponent', () => {
  let component: SaluladorComponent;
  let fixture: ComponentFixture<SaluladorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaluladorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaluladorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
