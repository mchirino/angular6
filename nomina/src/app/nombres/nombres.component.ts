import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Personal } from './../models/personal.model';


@Component({
  selector: 'app-nombres',
  templateUrl: './nombres.component.html',
  styleUrls: ['./nombres.component.css']
})
export class NombresComponent implements OnInit {
  @Input() perso: Personal;
  @HostBinding('attr.class') cssClass = 'col-md-a';

  constructor() {

  }
  ngOnInit() {
  }

}
