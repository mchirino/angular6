import { Component, OnInit, Input } from '@angular/core';
import { Personal } from './../models/personal.model';

@Component({
  selector: 'app-trabajadores',
  templateUrl: './trabajadores.component.html',
  styleUrls: ['./trabajadores.component.css']
})


export class TrabajadoresComponent implements OnInit {
  perso: Personal[];
  constructor() {
    this.perso = [];
  }

  ngOnInit() {
  }

  guardar(nombre: string, apellido: string): boolean {
    this.perso.push(new Personal(nombre, apellido));
    console.log(this.perso);

    //this.perso.push (new Personal(nombre, apellido));

    return false;
  }
}

